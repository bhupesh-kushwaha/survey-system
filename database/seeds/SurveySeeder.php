<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use \App\Survey;
use \App\Questions;

class SurveySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	if( !$this->defaultSurveyExists() ) {
	    	$survey = Survey::create(
				array(
					'title' => 'Survey Form',
					'total_pages' => '3'
				)
			);


			Questions::insert(array(
				array(
					'question' => 'Full Name',
					'type' => 'input',
					'is_required' => 1,
					'options' => '',
					'page_no' => 1,
					'survey_id' => $survey->id,
					'created_at' => Carbon::now(),
					'updated_at' => Carbon::now(),
				),
				array(
					'question' => 'Gender',
					'type' => 'radio',
					'is_required' => 1,
					'options' => serialize(
						array(
							'male' => "Male", 
							'female' => "Female"
						)
					),
					'page_no' => 1,
					'survey_id' => $survey->id,
					'created_at' => Carbon::now(),
					'updated_at' => Carbon::now(),
				),
				array(
					'question' => 'Date Of Birth',
					'type' => 'calender',
					'is_required' => 0,
					'options' => '',
					'page_no' => 1,
					'survey_id' => $survey->id,
					'created_at' => Carbon::now(),
					'updated_at' => Carbon::now(),
				),


				array(
					'question' => 'Full Address',
					'type' => 'textarea',
					'is_required' => 1,
					'options' => '',
					'page_no' => 2,
					'survey_id' => $survey->id,
					'created_at' => Carbon::now(),
					'updated_at' => Carbon::now(),
				),
				array(
					'question' => 'Address2 Optional',
					'type' => 'textarea',
					'is_required' => 0,
					'options' => '',
					'page_no' => 2,
					'survey_id' => $survey->id,
					'created_at' => Carbon::now(),
					'updated_at' => Carbon::now(),
				),


				array(
					'question' => 'City Name',
					'type' => 'select',
					'is_required' => 0,
					'options' => serialize(
						array(
							'ahmedabad' => "Ahmedabad",
							'bharuch' => "Bharuch", 
							'surat' => "Surat"
						)
					),
					'page_no' => 3,
					'survey_id' => $survey->id,
					'created_at' => Carbon::now(),
					'updated_at' => Carbon::now(),
				),

				array(
					'question' => 'Pincode',
					'type' => 'input',
					'is_required' => 1,
					'options' => '',
					'page_no' => 3,
					'survey_id' => $survey->id,
					'created_at' => Carbon::now(),
					'updated_at' => Carbon::now(),
				)
			));
		}
    }

    protected function defaultSurveyExists()
    {
        return Survey::whereTitle('Survey Form')->exists();
    }
}
