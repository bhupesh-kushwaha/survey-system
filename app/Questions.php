<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Questions extends Model
{
	const TAG_INPUT = "input";
	const TAG_RADIO = "radio";
	const TAG_SELECT = "select";
	const TAG_CALENDER = "calender";
	const TAG_TEXTAREA = "textarea";

    public $fillable = [
    	'question', 
    	'type', 
    	'is_required', 
    	'options', 
    	'page_no', 
    	'survey_id'
    ];

    public function getOptionsAttribute($value) {
        return unserialize($value); 
    }

    public function questions(){
        return $this->hasMany('App\Answers', 'question_id', 'id');
    }
}
