<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\FormHelper;
use App\Survey;
use Response;
use App\Questions;
use App\Answers;
use Auth;

class HomeController extends Controller
{
    protected $formHelper;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(FormHelper $formHelper)
    {
        $this->middleware('auth');

        $this->formHelper = $formHelper;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index($page = null, Request $request) 
    {

        if( is_null($page) ) {
            $page = 1;
        }

        if( $request->ajax() ) {
            $page = $request->current_page;
        }

        $isNewAnswer = 0;

        $isSurveyCompleted = false;
        
        $surveyStatus = Survey::SURVEY_STATUS_COMPLETED;

        $survey = Survey::findOrFail(Survey::DEFAULT_SURVEY_ID);

        if( $this->formHelper->getAnswer($survey->total_pages)->count() !== 0 ) {
            $isSurveyCompleted = true;
            $surveyForm = $this->formHelper->getAnswerComplatePage();

            goto jumpReturn;
        }

        $getAnswer = $this->formHelper->getAnswer($page);

        if( $getAnswer->count() === 0 ){
            $surveyStatus = Survey::SURVEY_STATUS_PENDING;

            $isNewAnswer = 1;
        }

        $surveyForm = $this->formHelper->getForm($page, $survey->id, $getAnswer);

        if( $request->ajax() ) {

            $answer = $this->formHelper->saveAnswer($request);

            $url = $this->formHelper->getNextUrl($page, $survey->total_pages);

            return $this->formHelper->generaeResponse(
                200, 
                'success', 
                'Data saved successfully', 
                array( 'nextUrl' => $url ) 
            );
        }

        jumpReturn:

        return view('home', [
            'survey_title' => $survey->title,
            'survey_total_pages' => $survey->total_pages,
            'survey_form' => $surveyForm,
            'current_page' => $page,
            'survey_status' => $surveyStatus,
            'survey_status_class' => ($surveyStatus == Survey::SURVEY_STATUS_PENDING) ? "text-danger" : "text-success",
            'is_new_answer' => $isNewAnswer,
            'survey_completed' => $isSurveyCompleted
        ]);
    }
}
