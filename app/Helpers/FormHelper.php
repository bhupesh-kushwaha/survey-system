<?php

namespace App\Helpers;

use App\Survey;
use App\Questions;
use App\Answers;
use Response;
use Auth;

class FormHelper implements FormHelperInterface {

	public function getForm($page_id, $survey_id, $old_answer) {

		$getQuestion = $this->getQuestion($page_id, $survey_id)->get();

		$generateForm = $this->generateForm($getQuestion, $old_answer);

		return $generateForm;
	}


	public function getQuestion($page_id, $survey_id) {
		return Questions::where([
			'survey_id' => $survey_id,
			'page_no' => $page_id
		]);
	}

	public function generateForm($questions, $old_answer) {

		$html = '';

		foreach ($questions as $key => $value) {

			$questionID = $value->id;

			$filterAnswer = [];

			if( $old_answer->count() !== 0 ) {
				$filterAnswer = $old_answer->filter(function ($value, $key) use ($questionID){
		        	return $value->question_id == $questionID;
		    	})->toArray();

		    	$filterAnswer = current($filterAnswer);
			}

			$html .= $this->getFormControls( $value->type, $value, $filterAnswer );
		}

		return $html;
	}

	public function getFormControls($type, $option, $old_answer) {
		$control = "";
		switch ($type) {
	        case Questions::TAG_INPUT:
	            $control = view('FormControls.input',['options' => $option, 'old_answer' => $old_answer])->render();
	            break;

	        case Questions::TAG_RADIO:
	        	$control = view('FormControls.radio',['options' => $option, 'old_answer' => $old_answer])->render();
	            break;

	        case Questions::TAG_SELECT:
	        	$control = view('FormControls.select',['options' => $option, 'old_answer' => $old_answer])->render();
	            break;
	            
	        case Questions::TAG_CALENDER:
	        	$control = view('FormControls.calender',['options' => $option, 'old_answer' => $old_answer])->render();
	            break;    

	        case Questions::TAG_TEXTAREA:
	        	$control = view('FormControls.textarea',['options' => $option, 'old_answer' => $old_answer])->render();
	            break;            
    	}

    	return $control;
	}

	public function saveAnswer($request) {

        $isNewAnswer = $request->is_new_answer;
        
        foreach ($request->except('current_page', 'is_new_answer') as $key => $value) {

            $keyArray = explode('-', $key);

            if( is_array($keyArray) ) {

                if( $isNewAnswer == 0 ) {
                    $answer = Answers::find($keyArray[3]);
                    $answer->answer = $value;
                    $answer->user_id = Auth::user()->id;
                    $answer->question_id = $keyArray[2];
                    $answer->save();
                }
                else {
                    $answer =  new Answers;
                    $answer->answer = $value;
                    $answer->user_id = Auth::user()->id;
                    $answer->question_id = $keyArray[2];
                    $answer->save();
                }
            }
        }

        return true;
    }

    public function getNextUrl($page, $total_pages) {
        $url = route('home', ['page' => ($page + 1)]); 

        if( $page == $total_pages ) {
            $url = route('home', ['page' => '']); 
        }

        return $url;
    } 

    public function getAnswer($page){
        return Answers::selectRaw('answers.*')
            ->join('questions' , 'answers.question_id' ,'=' , 'questions.id')
            ->join('surveys' , 'questions.survey_id' , '=' , 'surveys.id')
            ->where( [
                'answers.user_id' => Auth::user()->id,
                'questions.page_no' => $page,
                'surveys.id' => Survey::DEFAULT_SURVEY_ID
            ])->get();
    }

    public function generaeResponse($code, $status, $msg, $data ) {
    	return response()->json([
            'status' => 'success',
            'message' => 'Data saved successfully',
            'data' => $data
        ], $code);
    }

    public function getAnswerComplatePage() {

    	$answers = Answers::selectRaw('answers.answer, questions.question')
            ->join('questions' , 'answers.question_id' ,'=' , 'questions.id')
            ->join('surveys' , 'questions.survey_id' , '=' , 'surveys.id')
            ->where( [
                'answers.user_id' => Auth::user()->id,
                'surveys.id' => Survey::DEFAULT_SURVEY_ID
            ])->get();

    	return view('surveyComplate',['lists' => $answers])->render();
    }
}