<?php

namespace App\Helpers;

interface FormHelperInterface {

	public function getForm($page_id, $survey_id, $old_answer);

	public function getQuestion($page_id, $survey_id);

	public function generateForm($questions, $old_answer);

	public function getFormControls($type, $option, $old_answer);

	public function saveAnswer($request);

	public function getNextUrl($page, $total_pages);

	public function getAnswer($page);

	public function generaeResponse($code, $status, $msg, $data ) ;

	public function getAnswerComplatePage();
}