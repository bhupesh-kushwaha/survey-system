<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Survey extends Model
{
	const DEFAULT_SURVEY_ID = 1;
	const SURVEY_STATUS_PENDING = "pending"; 
	const SURVEY_STATUS_COMPLETED = "completed"; 
	
    public $fillable = [
    	'title', 
    	'total_pages'
    ];

    public function questions(){
    	return $this->hasMany('App\Questions', 'survey_id', 'id');
	}
}
