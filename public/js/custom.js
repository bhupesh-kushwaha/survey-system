var postSurvey = function($url, $action, $surveyFormData) {
    $.ajax({
        type: 'post',
        url: $action,
        dataType: 'json', 
        data: $surveyFormData,
        beforeSend: function() {
        	$('.loading').removeClass('d-none');
    	},
        success: function(data) {
           
           	console.log(data);

            if (data.status === 'success') {

                window.location.href = data.data.nextUrl;
            }
        },
        error: function(xhr) { 
	        $('.loading').addClass('d-none');
	    },
	    complete: function() {
	        $('.loading').addClass('d-none');
	    }
    });
}

var formValidation = function() {
        var isRequired = false;
        var error_log = "";
        $( '#surveyForm' ).find( 'select, textarea, input' ).each(function(){
            if( $(this).prop( 'required' )){

				if ( !$(this).val() ) {
                    isRequired = true;
                    error_log = $(this).attr('name') + " is required.";

                    if($(this).closest('.form-group').find('small').length == 0) {
                    	$(this).closest('.form-group').find('label:first').append(
                    		'<small class="error-msg form-text text-danger">'+error_log+'</small>'
                    	);
                	}
                }
            } 
            else {
				$(this).closest('.form-group').find('label').remove('small');
            }
        });

	return isRequired;        
}

$(document).ready(function(){ 

	$('.calendar').datepicker({ 
		autoclose: true, 
		todayHighlight: true
	});

    $('.select2').select2({
        placeholder: 'Select an option'
    });

    $(document).on('click', '.btnSubmit', function(e) {

    	e.preventDefault();

    	var currentPage = $(this).data('page');

    	var actionUrl = $('#surveyForm').data('url');
    	
    	var postUrl = $('#surveyForm').data('action');

    	if( $(this).hasClass('back') ) {
    		window.location.href = actionUrl + "/" + currentPage;
    	}

		var validation = formValidation();

    	if( validation == true) {
    		return false;
    	}

    	var surveyFormData  = $('#surveyForm').serialize();

        postSurvey( actionUrl + "/" + currentPage , postUrl, surveyFormData );
    })
}); 