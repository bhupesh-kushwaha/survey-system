@php
	$old_answer = ( !empty($old_answer) ) ? $old_answer : "" ;
	$answerId = ( $old_answer != "" ) ? "-" . $old_answer['id'] : "";
	$answervalue = ( $old_answer != "" ) ? $old_answer['answer'] : "";
@endphp

<div class="form-group">
  <label for="textarea{{ $options->id }}">{{ $options->question }}</label>
  
  <textarea class="form-control" 
  	rows="5" 
  	id="textarea{{ $options->id }}" 
  	name="q-{{ $options->survey_id .'-'.$options->id . $answerId }}" 
  	{{ ( $options->is_required === 1 ) ? "required" : "" }}
  >{{ $answervalue }}</textarea>
</div>