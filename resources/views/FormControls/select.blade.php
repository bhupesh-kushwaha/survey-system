@php
  $old_answer = ( !empty($old_answer) ) ? $old_answer : "" ;
  $answerId = ( $old_answer != "" ) ? "-" . $old_answer['id'] : "";
  $answervalue = ( $old_answer != "" ) ? $old_answer['answer'] : "";
@endphp

<div class="form-group">
  <label for="select{{ $options->id }}">{{ $options->question }}</label>
  
  <select class="form-control select2" 
  	id="select{{ $options->id }}" 
  	name="q-{{ $options->survey_id .'-'.$options->id . $answerId }}"
  	{{ ( $options->is_required === 1 ) ? "required" : "" }}
  >
  	<option value="">Select </option>
  	@foreach($options->options as $key => $value)
    	<option value="{{ $key }}" {{ ( $answervalue ==  $key ) ? "selected" : "" }}> {{ $value }} </option>
    @endforeach

  </select>
</div>