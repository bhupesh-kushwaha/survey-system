@php
	$old_answer = ( !empty($old_answer) ) ? $old_answer : "" ;
	$answerId = ( $old_answer != "" ) ? "-" . $old_answer['id'] : "";
	$answervalue = ( $old_answer != "" ) ? $old_answer['answer'] : "";
@endphp

<div class="form-group">
	<label for="radio{{ $options->id }}">{{ $options->question }}</label>

	@php $i = 0; @endphp
	@foreach($options->options as $key => $value)
		<div class="form-check-inline">

		  <label class="form-check-label">

		    <input type="radio" 
		    	class="form-check-input" 
		    	name="q-{{ $options->survey_id .'-'.$options->id . $answerId}}"
		    	value="{{$key}}" 
		    	{{ ( $options->is_required === 1 ) ? "required" : "" }}
		    	{{ ( $answervalue ==  $key ) ? "checked" : "" }}
		    >{{ $value }}
		  </label>
		  
		</div>
		@php $i++ @endphp
	@endforeach	

</div>