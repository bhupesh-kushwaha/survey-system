@php
	$old_answer = ( !empty($old_answer) ) ? $old_answer : "" ;
	$answerId = ( $old_answer != "" ) ? "-" . $old_answer['id'] : "";
	$answervalue = ( $old_answer != "" ) ? $old_answer['answer'] : "";
@endphp

<div class="form-group">
  <label for="input{{ $options->id }}">{{ $options->question }}</label>
  
  <input type="text" 
  	class="form-control calendar" 
  	id="input{{ $options->id }}" 
  	name="q-{{ $options->survey_id .'-'.$options->id . $answerId}}" 
  	{{ ( $options->is_required === 1 ) ? "required" : "" }}
  	value="{{ $answervalue }}"
  />
</div>