@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-10">

            @php
                $progress = round( $current_page * 100 / $survey_total_pages);

                if( $survey_completed ) {
                    $progress = 100;
                }
            @endphp

            <div class="progress mb-2">
                <div class="progress-bar progress-bar-striped bg-success" 
                    role="progressbar" 
                    style="width: {{ $progress }}%;" 
                    aria-valuenow="{{ $progress }}" 
                    aria-valuemin="0" 
                    aria-valuemax="100"
                >
                    {{ $progress }}%
                </div>
            </div>

            <form id="surveyForm" data-url="{{ route('home') }}" data-action="{{ route('home-ajax') }}" >

                <input type="hidden" name="current_page" value="{{ $current_page }}" >
                <input type="hidden" name="is_new_answer" value="{{ $is_new_answer }}" >

                <div class="card">
                    <div class="card-header text-center">
                        <span class="float-left">
                            Survay : <strong>{{ $survey_title }}</strong>
                        </span>

                        Status: <strong class={{ $survey_status_class }}>{{ $survey_status }}</strong>   

                        @if( !$survey_completed )
                            <span class="float-right">
                                Page {{ $current_page }} of {{ $survey_total_pages }}
                            </span>
                        @endif
                    </div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <div class="row">
                            <div class="col-md-12">
                                
                                {!! $survey_form !!}
                                
                            </div>
                        </div>

                    </div>

                    <div class="card-footer">
                        <div class="row">
                            <div class="col-md-12">
                                @if( !$survey_completed )
                                    @php
                                        $backDisabled = ($current_page == 1) ? "disabled" : "";
                                    @endphp
                                    <button type="button" 
                                        class="btn btn-primary back btnSubmit"
                                        {{ $backDisabled }}
                                        data-page="{{ $current_page - 1 }}"
                                    >
                                        Back
                                    </button>

                                    <button type="button" 
                                        class="btn btn-success btnSubmit"
                                        data-page="{{ $current_page + 1 }}"
                                    >
                                        {{ ($current_page == $survey_total_pages) ? "Submit" : "Continue" }}
                                    </button>
                                @else
                                    <p>Note : Your servey completed</p>    
                                @endif
                            </div>
                        </div>
                    </div>
                </div>

            </form>
        </div>
    </div>
</div>
@endsection

@push('scripts')
    <!-- <script src="/custom.js"></script> -->
@endpush
